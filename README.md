# Git Merge Request verkefni
Fyrsta GitLab verkefnið þar sem við contribute-um á verkefni hjá öðrum. :) 

## Getting started
To start contributing to this project you should fork it to your own GitLab repository. Then you can clone it to your local repository on your machine. 

### Prerequisites
You will need to have Node.js on your machine and this document will assume that you have yarn (npm should be ok too). To install yarn do:
npm install -g yarn
you will also need to have jest (that is the testing framework: https://jestjs.io/docs/en/getting-started). To install it just run the yarn command in the terminal in your project directory with no arguments like so:
 yarn

 ### Opening the page in your browser
 Once you fork this library to your own GitLab a CI process will start runing and after it has passed you should be able to run the page under settings > pages

 ### How to contribute to the project
 If you want to add something to the project you will have to edit the main.js file.<br>
 First you have to add a character name to the names array, it can be any character you can think of.
    *The name must start with a capital letter.<br> 
 Next you want to add an image of that character into the images array. You should insert the URL to the image you want to use.<br> 
    *The image URL must contain "http".<br>

 *Just remember to add a comma after the previous line, befor you add your character name and image :)<br> 

 The div which contains your Character should contain an h2 and an im element, which it does unless you change the index file. So don't change the index file :)<br>

 This is it. If you have added your chapter text and chapter name to the correct places you will have made a successful contribution to the project<br>

## Running the tests

Before you commit your code and do a merge request you should test your code by typing

```yarn test```

Good luck and have a nice day :D 

